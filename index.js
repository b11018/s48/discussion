console.log('s48 - JS - Reactive DOM with JSON')

// Mock database
let posts = [];
// count variable that will serve as the post ID
let count = 1;

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// prevents the page from loading
	e.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});

	// Increments the id value in count variable
	count++;

	showPosts(posts);
	alert('Successfully added new post.');
});

// Show posts
const showPosts = (posts) => {
	// Sets the html structure to display new posts
	let postEntries = '';

	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button id="editBtn-${post.id}" onclick="editPost('${post.id}')">Edit</button>
				<button id="delBtn-${post.id}" onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries
}

// Edit post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
};

// Update post
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {
	e.preventDefault();

	/*
		posts[0].id = 1;
		posts[0].title = Inception;
		posts[0].body = Scifi;

		txt-edit-id = 1
		txt-edit-title = Anabelle;
		txt-edit-body = Horror;

		posts[0].id = 1 === #txt-edit-id.value = 1
	*/
	for(let i = 0; i < posts.length; i++){
		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert('Successfully updated');

			break;
		}
	}
});

// Delete post (ACTIVITY)

const deletePost = (id) => {
	// Initial Solution
	/*
	for(i = 0; i < posts.length; i++){
		if (posts[i].id === parseInt(id)){
			posts.splice(i, 1)
			
			showPosts(posts);
			alert('Successfully deleted');

			break;
		}		
	}
	*/
	// Alternative Solution
	let postIndex = posts.findIndex(i => i === id);
	posts.splice(postIndex, 1);

	showPosts(posts);
	alert('Successfully deleted');
};